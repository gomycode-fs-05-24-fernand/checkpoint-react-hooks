import React from 'react';
import './moviecard.css';
import { Link } from 'react-router-dom';

const MovieCard = ({movie}) => {

    return (
            <div  className="movieCard" style={{
                        backgroundImage: "url("+movie.posterUrl+")",
                        height: '400px',
                        width: '100%',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                        color:'white',
                        }}>
            <h2>{movie.title}</h2>
            <strong> Rating: {movie.rating} / 5</strong>
            <button className='movie-card-button'>
                <Link to={`/movie/?title=${movie.title}&description=${movie.description}&trailer=${movie.trailer}&poster=${movie.posterUrl}&rating=${movie.rating}`}>Voir Plus</Link>
            </button>

                    </div>
    );
}

export default MovieCard;
