import React, { useState } from 'react';
import './formmovie.css';

const FormMovie = ({ movies, setMovies }) => {

    const addMovie = (formData) => {
        const newMovie = {
            id: movies.length ? movies[movies.length - 1].id + 1 : 1,
            title: formData.get("title"),
            posterUrl: formData.get("posterUrl"),
            description: formData.get("description"),
            rating: formData.get("rating"),
            trailer: formData.get("trailer")
        };
        console.log(newMovie);
        // Utilisez setMovies pour mettre à jour l'état
        setMovies([...movies, newMovie]);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        let formData = new FormData(e.target);
        addMovie(formData);
    };

    return (
        <form onSubmit={handleSubmit}>
            <input type='text' placeholder='title' name='title' required />
            <br />
            <input type='text' placeholder='Poster URL' name='posterUrl' required />
            <br />
            <textarea name='description' placeholder='description' required />
            <br />
            <input type='number' min={0} max={5} placeholder='rating' name='rating' required />
            <br />
            <input type='text'  placeholder='trailer' name='trailer' required />
            <br />
            <button type='submit'>Add movie</button>
        </form>
    );
};

export default FormMovie;
