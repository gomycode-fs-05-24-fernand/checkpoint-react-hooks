import React from 'react';
import MovieCard from './MovieCard/MovieCard';

import './movieList.css';

const MovieList = ({movies}) => { 

    return (
        
        <div>
            <div id='movieList'>
                {movies.map(movie => (
                    <MovieCard key={movie.id} movie={movie} />
                ))}
            </div>
        </div>
    )
}

export default MovieList;