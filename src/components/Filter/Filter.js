import React, { useState, useEffect } from 'react';
import MovieList from '../MovieList/MovieList';

const Filter = ({ movies }) => {
    const [titleFilter, setTitleFilter] = useState('');
    const [ratingFilter, setRatingFilter] = useState('');
    const [moviesFiltered, setMoviesFiltered] = useState(movies);

    useEffect(() => {
        const filtered = movies.filter(movie => 
            movie.title.toLowerCase().includes(titleFilter.toLowerCase()) &&
            movie.rating.toLowerCase().includes(ratingFilter.toLowerCase())
        );
        setMoviesFiltered(filtered);
    }, [titleFilter, ratingFilter, movies]);

    return (
        <div>
            <input 
                type='text' 
                name='filter'  
                placeholder='Title' 
                id="title-filter" 
                value={titleFilter}
                onChange={(e) => setTitleFilter(e.target.value)} 
            />
            <input 
                type='number' 
                name='filter' 
                placeholder='Rating' 
                id='rating-filter' 
                min={0} 
                max={5} 
                value={ratingFilter}
                onChange={(e) => setRatingFilter(e.target.value)} 
            />
            <MovieList movies={moviesFiltered.length > 0 ? moviesFiltered : movies} />
        </div>
    );
}

export default Filter;
