import React,{useState} from 'react';

const TestInput = () => {
    const [firstName,setFirstName] = useState('Fernand');

    const handleChange = (e) => {
        setFirstName(e.target.value)
    }
    return (
        <form>
            <input type='text' name='firstname' value={firstName} onChange={handleChange}/>
            <p>{firstName}</p>
        </form>

        
    );
}

export default TestInput;
