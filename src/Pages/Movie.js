import React from 'react';
import './movie.css';
import { useLocation, Link } from 'react-router-dom';

const Movie = () => {

    const location = useLocation();

    const searchParams = new URLSearchParams(location.search);
    const title = searchParams.has("title") ? searchParams.get("title") : "No title";
    const description =  searchParams.has("description") ? searchParams.get("description") : "No description";
    const trailer =  searchParams.has("trailer") ? searchParams.get("trailer") : null;
    const rating =  searchParams.has("rating") ? searchParams.get("rating") : "no rating";
    const poster =  searchParams.has("poster") ? searchParams.get("poster") : null;

    console.log(description);
    return (
    <div className='movie-page'>
        <Link to='/' className='backlink'>Retour</Link>
        <div className='movie'>
            <img src={poster} alt={title}/>
            <h1>{title}</h1>
            <strong>Rating: {rating} / 5</strong>
            <div className='description'>
            <p>{description}</p>
            </div>

            <iframe  src={trailer} title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin" allowFullScreen></iframe>
        </div>
    </div>
    );
}

export default Movie;
