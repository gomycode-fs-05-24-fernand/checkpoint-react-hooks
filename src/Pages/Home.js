import React, {useState} from 'react';

import FormMovie from '../components/MovieList/FormMovie/FormMovie';
import Filter from '../components/Filter/Filter';

const Home = () => {
    
    const [movies,setMovies] = useState([]);
  
    return (
        <div className='home'>
            <div className='left'>
                <h1>My movie List</h1>
                <FormMovie movies={movies} setMovies={setMovies}/>
            </div>
            <div className='right'>
                <Filter movies={movies} />
            </div>
        </div>
    );
  }




export default Home;
