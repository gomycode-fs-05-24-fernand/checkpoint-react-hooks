import React , {useState} from 'react';
import Home from './Pages/Home';
import Movie from './Pages/Movie';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import './App.css';



function App() {

  const [movies,setMovies] = useState([]);


  return (
    <div className="App">
      <Router>
        <Routes>
          <Route index path='/' element={<Home/>}></Route>
          <Route path='/movie/*' element={<Movie/>}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;




